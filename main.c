#include "bmp.h"
#include <stdio.h>
#include <stdlib.h>


char *input_filename = "Lights.bmp";
char *output_filename = "output.bmp";

void invoke_function(struct image (*function)(struct image), struct image img_in, struct image image_out, char* description);

void default_image();
void big_image();
        int main() {
            default_image();
big_image();
        }



void default_image(){
    FILE *inBmp = fopen(input_filename, "rb");
    FILE *outBmp = fopen(output_filename, "wb+");
    struct image image;
    struct image result1;
    struct image result2;

    enum read_status read_status = from_bmp(inBmp, &image);
    if (read_status != READ_OK) print_error_read(read_status);
    result1 = sepiaFilterAsm(image);
    invoke_function(sepiaFilterAsm, image, result1, "SSE");
    enum write_status write_status = to_bmp(outBmp, &result1);
    result2 = sepiaFilter(image);
    invoke_function(sepiaFilter, image, result2, "C");
    if (write_status != WRITE_OK) print_error_write(write_status);

    fclose(inBmp);
    fclose(outBmp);

        }

void big_image(){
    FILE *inBmp = fopen(input_filename, "rb");
    FILE *outBmp = fopen(output_filename, "wb+");
    struct image image;
    struct image result1_big;
    struct image result2_big;

    enum read_status read_status = from_bmp(inBmp, &image);
    if (read_status != READ_OK) print_error_read(read_status);
    result1_big = sepiaBigFilterAsm(image);
    invoke_function(sepiaFilterAsm, image, result1_big, "SSEB");
    enum write_status write_status = to_bmp(outBmp, &result1_big);
    result2_big = sepiaBigFilter(image);
    invoke_function(sepiaFilter, image, result2_big, "CB");
    if (write_status != WRITE_OK) print_error_write(write_status);

    fclose(inBmp);
    fclose(outBmp);

}

void invoke_function(struct image (*function)(struct image), struct image img_in, struct image image_out, char* description) {
    struct rusage r;
    struct timeval start;
    struct timeval end;
    getrusage(RUSAGE_SELF, &r);
    start = r.ru_utime;

    function(img_in);


    getrusage(RUSAGE_SELF, &r);
    end = r.ru_utime;

    double res_time = ((end.tv_sec - start.tv_sec) * 1000000L) + end.tv_usec - start.tv_usec;
    printf("Time elapsed in seconds for %s filter: %.2f\n", description, res_time/1000000.0);

}

